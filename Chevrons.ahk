!x::Send {U+00AB}
!y::Send {U+00BB}
!c::Send {U+203A}
!v::Send {U+2039}


; #	Win (Windows logo key)
; !	Alt
; ^	Control
; +	Shift
; &	An ampersand may be used between any two keys or mouse buttons to combine them into a custom hotkey.